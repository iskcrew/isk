require_relative "../cli_helpers.rb"

class HttpSlideTask < BackgroundTask
  def self.perform
    say "Fetching http-slides.."
    slides = []
    realtime = Benchmark.realtime do
      slides = Event.current.slides.where(type: "HttpSlide").all.each do |s|
        begin
          s.generate_images
        rescue StandardError => e
          say "Error fetching http-slide: #{s.name}"
          puts e.message
          puts e.backtrace.inspect
        end
      end
    end
    say " -> Fetched #{slides.size} slides in %.2f seconds (%.2f sec. per slide)" % [realtime, realtime / slides.size]
  end
end