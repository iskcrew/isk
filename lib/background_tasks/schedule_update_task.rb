class ScheduleUpdateTask < BackgroundTask
  def self.perform
    say "Generating schedule slides.."
    schedules = []
    realtime = Benchmark.realtime do
      schedules = Event.current.schedules.all.each(&:generate_slides)
    end
    say(" -> Generated #{schedules.size} schedules in %.2f seconds (%.2f sec. per schedule)" % [realtime, realtime / schedules.size])
  end
end