# Container images for isk

This directory contains configuration for building container images for isk.

## base

This is the "base" image containing isk and all it's dependencies.

## postgres

Postgresql image, initializes databases isk and isk-test.