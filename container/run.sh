#!/bin/bash
docker run --name isk-postgres -e POSTGRES_PASSWORD=isk -d postgres:9
docker run --name isk-redis -d redis
docker run --name isk-memcache -d memcached
docker run --name isk \
	--link isk-postgres:postgres \
	--link isk-redis:redis \
	--link isk-memcache:memcached \
	-e ISK_SECRET_KEY_BASE=70738cfefa9e69c261a416aadddb6cc3224f945d9d59ac48652ec3c61f9ec433e58c55642303ce7cdef0ffb18fa789c4f3dc94266dfbbfd4facf4cad00bb373f \
	--rm -t registry.gitlab.com/iskcrew/isk/app