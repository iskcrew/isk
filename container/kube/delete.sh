#!/bin/bash
kubectl delete -f volumes/postgres_volumes.yaml
kubectl delete -f volumes/app_volumes.yaml
kubectl delete -f services/postgres_svc.yaml
kubectl delete -f services/redis_svc.yaml
kubectl delete -f services/memcache_svc.yaml
kubectl delete -f services/rails_svc.yaml
kubectl delete -f deployments/postgres_deploy.yaml
kubectl delete -f jobs/setup.yaml
kubectl delete -f deployments/redis_deploy.yaml
kubectl delete -f deployments/memcache_deploy.yaml
kubectl delete -f deployments/resque_deploy.yaml
kubectl delete -f deployments/rails_deploy.yaml
kubectl delete -f ingresses/ingress.yaml
