#!/bin/bash
kubectl create -f volumes/postgres_volumes.yaml
kubectl create -f volumes/app_volumes.yaml
kubectl create -f services/postgres_svc.yaml
kubectl create -f services/redis_svc.yaml
kubectl create -f services/memcache_svc.yaml
kubectl create -f services/rails_svc.yaml
kubectl create -f deployments/postgres_deploy.yaml
kubectl create -f jobs/setup.yaml
kubectl create -f deployments/redis_deploy.yaml
kubectl create -f deployments/memcache_deploy.yaml
kubectl create -f deployments/resque_deploy.yaml
kubectl create -f deployments/rails_deploy.yaml
kubectl create -f ingresses/ingress.yaml
