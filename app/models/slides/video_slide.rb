# frozen_string_literal: true

# ISK - A web controllable slideshow system
#
# Author::    Vesa-Pekka Palmu
# Copyright:: Copyright (c) 2012-2013 Vesa-Pekka Palmu
# License::   Licensed under GPL v3, see LICENSE.md

class VideoSlide < Slide
  TypeString = "video"
  FilePath = Rails.root.join("data", "video")
  VideoThumbnail = Rails.root.join("data", "video", "no_video.png")

  DefaultSlidedata = ActiveSupport::HashWithIndifferentAccess.new(
    url: "http://"
    ).freeze
  include HasSlidedata

  after_create do |s|
    s.send(:write_slidedata)
  end

  after_initialize do
    self.is_svg = false
    self.show_clock = false
    self.duration = 0
    true
  end

  def clone!
    new_slide = super
    new_slide.slidedata = slidedata
    return new_slide
  end

  private
  def generate_full_image
    # TODO generate video thumbnail
    FileUtils.cp VideoThumbnail, full_filename
    true
  end
end
