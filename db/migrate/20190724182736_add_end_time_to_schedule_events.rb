class AddEndTimeToScheduleEvents < ActiveRecord::Migration[4.2]
  def change
    add_column :schedule_events, :end_time, :datetime
  end
end
