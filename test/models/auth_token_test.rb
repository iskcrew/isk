# frozen_string_literal: true

require "test_helper"

class AuthTokenTest < ActiveSupport::TestCase
  def setup
    @adminsession = { user_id: users(:admin).id, username: users(:admin).username }
  end

  test "unset token auth" do
    token=auth_tokens(:one)
    user = AuthToken.authenticate("invalid")
    assert_not user, "User should not be found for invalid token"
    u = User.find(1)
  end

  test "correct token auth" do
    token=auth_tokens(:one)
    u = User.find(token.user_id)
    u.auth_tokens.append token
    user = AuthToken.authenticate(token.token)
    assert user, "User should be found for token"
    assert_equal user.id, token.user_id
  end
end
