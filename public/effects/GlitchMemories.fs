uniform sampler2D u_empty;
uniform sampler2D u_from;
uniform sampler2D u_to;
uniform float u_time;
uniform float u_transition_time;

varying vec2 v_uv;

#define progress u_transition_time
vec4 getFromColor(vec2 p) {return texture2D(u_from, p);}
vec4 getToColor(vec2 p) {return texture2D(u_to, p);}

// ==================================================================
// https://gl-transitions.com/editor/GlitchMemories
// author: Gunnar Roth
// based on work from natewave
// license: MIT

vec4 transition(vec2 p) {
  vec2 block = floor(p.xy / vec2(16));
  vec2 uv_noise = block / vec2(64);
  uv_noise += floor(vec2(progress) * vec2(1200.0, 3500.0)) / vec2(64);
  vec2 dist = progress > 0.0 ? (fract(uv_noise) - 0.5) * 0.3 *(1.0 -progress) : vec2(0.0);
  vec2 red = p + dist * 0.2;
  vec2 green = p + dist * .3;
  vec2 blue = p + dist * .5;

  return vec4(mix(getFromColor(red), getToColor(red), progress).r,mix(getFromColor(green), getToColor(green), progress).g,mix(getFromColor(blue), getToColor(blue), progress).b,1.0);
}

// ==================================================================

void main() {
    gl_FragColor = transition(v_uv);
}



