uniform sampler2D u_empty;
uniform sampler2D u_from;
uniform sampler2D u_to;
uniform float u_time;
uniform float u_transition_time;

varying vec2 v_uv;

#define progress u_transition_time
vec4 getFromColor(vec2 p) {return texture2D(u_from, p);}
vec4 getToColor(vec2 p) {return texture2D(u_to, p);}

// ====================================================================
// https://gl-transitions.com/editor/heart
// Author: gre
// License: MIT

float inHeart (vec2 p, vec2 center, float size) {
  if (size==0.0) return 0.0;
  vec2 o = (p-center)/(1.6*size);
  float a = o.x*o.x+o.y*o.y-0.3;
  return step(a*a*a, o.x*o.x*o.y*o.y*o.y);
}
vec4 transition (vec2 uv) {
  return mix(
    mix(getFromColor(uv),vec4(1.0,0.0,1.0,0.0), progress ),
    getToColor(uv),
    inHeart(uv, vec2(0.5, 0.4), progress)
  );
}


// ====================================================================

void main() {
    gl_FragColor = transition(v_uv);
}

