require 'net/http'
require 'uri'
require 'open-uri'
require 'base64'
require_relative '../cli_helpers.rb'

class AssemblyNetworkStatsTask < BackgroundTask
  # Netcrew rrd slides
  #Netcrew_ids = [1803, 1804] # Summer
  Netcrew_ids = [652, 653] # Winter
  # Netcrew urls
  Netcrew_urls = [
    'http://netcrew.asm.fi/saliav/asmw18_event_12h.png',
    'http://netcrew.asm.fi/saliav/asmw18_event.png'
  ]

  def self.perform
    say 'Fetching netcrew stats'
    slides = [
      [Netcrew_ids.first, Netcrew_urls.first],
      [Netcrew_ids.last,  Netcrew_urls.last]
    ]
    slides.each do |s|
      picture = Net::HTTP.get(URI.parse(s.last))
      slide = Slide.find(s.first)
      svg = Nokogiri::XML(slide.svg_data)
      encoded = 'data:image/jpeg;base64,' + Base64.encode64(picture)
      picture_element = svg.css('image#net-rrd').last
      picture_element['xlink:href'] = encoded

      File.open(slide.svg_filename, 'w+') do |f|
        f.puts svg.to_xml
      end
      picture = nil
      picture_element = nil
      svg = nil
      slide.generate_images_later
      slide = nil
    end
  end
end