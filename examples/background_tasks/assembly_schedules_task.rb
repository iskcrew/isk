require 'net/http'
require 'uri'
require 'open-uri'
require_relative '../cli_helpers.rb'


class AssemblySchedulesTask < BackgroundTask
  Schedule_url = URI.parse('http://schedule.assembly.org/winter18/schedules/events.json')
  OW_Schedule = 11
  CS_Schedule = 8
  StreamCorner_Schedule = 7
  # Summer master schedule = 2, Winter = 6
  Master_Schedule = 2
  SceneLounge_Scedule = 9
  ARTech_Schedule = 3

  def self.perform
    # Config

    say "Fetching Assembly schedule from #{Schedule_url}"
    begin
      schedule_data = Net::HTTP.get(Schedule_url)
      json = JSON.parse(schedule_data)

      schedule = Schedule.find(Master_Schedule)
      say "Updating major events schedule: #{schedule.name} (id=#{schedule.id})"
      begin
        schedule.schedule_events.destroy_all
        json["events"].each do |entry|
          if entry['flags'].include?('major') or entry['flags'].include?('bigscreen')
            next if entry['flags'].include?('cancelled')
            set_event_data(schedule, entry)
          end
        end
      rescue StandardError => e
        say "ERROR updating master schedule"
        puts e.message
        puts e.backtrace.join("\n")
      end

      begin
        if ARTech_Schedule
          schedule = Schedule.find(ARTech_Schedule)
          say "Updating ARTech schedule: #{schedule.name} (id=#{schedule.id})"
          schedule.schedule_events.destroy_all
          json["events"].each do |entry|
            if entry['categories'].include?('Seminar')
              next if entry['flags'].include?('cancelled')

              event = schedule.schedule_events.where(external_id: entry['key']).first_or_initialize
              event.name = entry['name'].split('ARTtech seminar:').last.strip
              next if event.name.blank?
              event.at = Time.parse(entry['start_time'])
              event.end_time = Time.parse(entry['end_time'])
              event.save!
            end
          end
        end
      rescue StandardError => e
        say "ERROR updating ARTech schedule"
        puts e.message
        puts e.backtrace.join("\n")
      end

      begin
        if OW_Schedule
          schedule = Schedule.find(OW_Schedule)
          say "Updating ow_stage schedule: #{schedule.name} (id=#{schedule.id})"
          schedule.schedule_events.destroy_all
          json["events"].each do |entry|
            if entry['location_key'] == 'ow_stage'
              next if entry['flags'].include?('cancelled')
              set_event_data(schedule, entry)
            end
          end
        end
      rescue StandardError => e
        say "ERROR updating ow_stage schedule"
        puts e.message
        puts e.backtrace.join("\n")
      end

      begin
        if CS_Schedule
          schedule = Schedule.find(CS_Schedule)
          say "Updating cs_stage schedule: #{schedule.name} (id=#{schedule.id})"
          schedule.schedule_events.destroy_all
          json["events"].each do |entry|
            if entry['location_key'] == 'stage'
              next if entry['flags'].include?('cancelled')
              set_event_data(schedule, entry)
            end
          end
        end
      rescue StandardError => e
        say "ERROR updating ow_stage schedule"
        puts e.message
        puts e.backtrace.join("\n")
      end

      begin
        if StreamCorner_Schedule
          schedule = Schedule.find(StreamCorner_Schedule)
          say "Updating stream schedule: #{schedule.name} (id=#{schedule.id})"
          schedule.schedule_events.destroy_all
          json["events"].each do |entry|
            if entry['location_key'] == 'stream_corner'
              next if entry['flags'].include?('cancelled')
              next if entry['name'].include? "Stream station:"

              event = schedule.schedule_events.where(external_id: entry['key']).first_or_initialize
              event.name = entry['name'].sub("Streamcorner:", "").strip
              event.at = Time.parse(entry['start_time'])
              event.at = Time.parse(entry['end_time'])
              event.save!
            end
          end
        end
      rescue StandardError => e
        say "ERROR updating stream corner schedule"
        puts e.message
        puts e.backtrace.join("\n")
      end

    rescue StandardError => e
      say "ERROR fetching schedule json"
      puts e.message
      puts e.backtrace.join("\n")
    end
  end

  private

  def self.set_event_data(schedule, entry)
    event = schedule.schedule_events.where(external_id: entry['key']).first_or_initialize
    event.name = entry['name'].strip
    event.at = Time.parse(entry['start_time'])
    event.end_time = Time.parse(entry['end_time'])
    event.save!
  end
end
