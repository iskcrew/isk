require 'net/http'
require 'uri'
require 'open-uri'
require_relative '../cli_helpers.rb'

# Fetches a schedule from horaro.org, service used by speedrunners

class HoraroOrgScheduleTask < BackgroundTask
  ScheduleURL = "https://horaro.org/-/api/v1/schedules/23112ze6kl83y97a6e"
  ScheduleID = 10

  def self.perform
    schedule = Schedule.find(ScheduleID)
    return unless schedule

    say "Fetching horaro.org schedule from #{ScheduleURL}"
    data = URI.parse(ScheduleURL).read
    json = JSON.parse(data)

    schedule.schedule_events.delete_all

    json['data']['items'].each do |item|
      event = schedule.schedule_events.new
      game, platform, category, player = *item['data']
      if player.present?
        event.name = "#{game} - #{player}"
      else
        event.name = "#{game}"
      end
      event.at = DateTime.parse item['scheduled']
      event.end_time = event.at + item['length_t'].seconds
      event.save!
    end
  end
end